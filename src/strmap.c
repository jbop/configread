#include "strmap.h"
#include <stdlib.h>
#include <string.h>

struct strmap_t {
	const char *key;
	void *value;
	struct strmap_t *lft;
	struct strmap_t *rgt;
};

/*******************
 * TREE MAP HANDLING
 *******************/

int strmap_add(struct strmap_t **map, const char *key, void *value) {
	int diff;
	if (*map == NULL) {
		*map = calloc(1, sizeof(struct strmap_t));
		if(*map == NULL) {
			return STRMAP_MEMERR;
		}
		(*map)->key = key;
		(*map)->value = value;
		return STRMAP_OK;
	}
	diff = strcmp((*map)->key, key);
	if (diff < 0) {
		return strmap_add(&(*map)->lft, key, value);
	}
	if (diff > 0) {
		return strmap_add(&(*map)->rgt, key, value);
	}
	return STRMAP_DUP;
}

void *strmap_get(struct strmap_t *map, const char *key) {
	int diff;
	if (map == NULL) {
		return NULL;
	}
	diff = strcmp(map->key, key);
	if (diff < 0) {
		return strmap_get(map->lft, key);
	}
	if (diff > 0) {
		return strmap_get(map->rgt, key);
	}
	return map->value;
}
void strmap_destroy(struct strmap_t *map, void (*free_fnc)(const char*, void*, void*), void *ctx) {
	if (map == NULL) {
		return;
	}
	strmap_destroy(map->lft, free_fnc, ctx);
	strmap_destroy(map->rgt, free_fnc, ctx);
	if (free_fnc != NULL) {
		(*free_fnc)(map->key, map->value, ctx);
	}
	free(map);
}

void strmap_iterate(struct strmap_t *map, void (*iterate_fnc)(const char*, void*, void*), void *ctx) {
	if (map != NULL) {
		strmap_iterate(map->lft, iterate_fnc, ctx);
		(*iterate_fnc)(map->key, map->value, ctx);
		strmap_iterate(map->rgt, iterate_fnc, ctx);
	}
}
