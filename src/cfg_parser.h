#ifndef CFG_PARSER_H_
#define CFG_PARSER_H_

#include "strmap.h"

struct cfg_parser_t;
typedef int (*cfg_parser_callback_t)(const char *, struct strmap_t *, void *);

struct cfg_parser_t *cfg_parser_create(void);
void cfg_parser_destroy(struct cfg_parser_t *cp);

int cfg_parser_register_handler(struct cfg_parser_t *cp, char *objtype,
		cfg_parser_callback_t callback);
int cfg_parser_parse(struct cfg_parser_t *cp, char *filename, void *ctx);

void cfg_parser_destroy_args(struct strmap_t *args);

#endif
