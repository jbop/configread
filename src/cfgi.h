#ifndef CFG_INHERITANCE_H_
#define CFG_INHERITANCE_H_

struct cfgi_t;
struct cfgi_obj_t;
typedef int (*cfgi_callback_t)(const char *, struct cfgi_obj_t *, void *);

struct cfgi_t *cfgi_create(void);
void cfgi_destroy(struct cfgi_t *cih);

int cfgi_register_handler(struct cfgi_t *cih, char *objtype,
		cfgi_callback_t callback);
void cfgi_parse(struct cfgi_t *cih, char *filename, void *ctx);

const char *cfgi_get_string(struct cfgi_obj_t *obj, const char *name);
long cfgi_get_long(struct cfgi_obj_t *obj, const char *name);
const char **cfgi_get_list(struct cfgi_obj_t *obj, const char *name);

#endif
