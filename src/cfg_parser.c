#include "cfg_parser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "strmap.h"

#define CFG_PARSER_BUF_SIZE 32768
#define CFG_PARSER_MIN_BUF_SIZE 16384


struct cfg_parser_t {
	/* parse state */
	FILE *fp;
	void *ctx;

	/* File access */
	char buf[CFG_PARSER_BUF_SIZE];
	size_t bufpos;
	size_t buflen;

	/* callbacks */
	struct strmap_t *callbacks;
};

static void cfg_parser_map_free_both_callback(const char*key, void*value, void*ctx);
static int cfg_parser_fill(struct cfg_parser_t *cp);
static size_t cfg_parser_read(struct cfg_parser_t *cp, char *buf, size_t len);
static size_t cfg_parser_strip_ws(struct cfg_parser_t *cp);
static char *cfg_parser_accept_name(struct cfg_parser_t *cp);
static char *cfg_parser_accept_line(struct cfg_parser_t *cp);
static int cfg_parser_accept_eol(struct cfg_parser_t *cp);
static int cfg_parser_accept_line_discard(struct cfg_parser_t *cp);
static int cfg_parser_accept_comment_discard(struct cfg_parser_t *cp);
static int cfg_parser_accept_word(struct cfg_parser_t *cp, char *word);
static int cfg_parser_accept_char(struct cfg_parser_t *cp, char c);
static int cfg_parser_parse_object(struct cfg_parser_t *cp);
static int cfg_parser_parse_entry(struct cfg_parser_t *cp);

/*******************
 * META FUNCTIONS
 *******************/
struct cfg_parser_t *cfg_parser_create(void) {
	return calloc(1, sizeof(struct cfg_parser_t));
}

void cfg_parser_destroy(struct cfg_parser_t *cp) {
	strmap_destroy(cp->callbacks, NULL, NULL);
	free(cp);
}

int cfg_parser_register_handler(struct cfg_parser_t *cp, char *objtype,
		cfg_parser_callback_t callback) {
	return strmap_add(&cp->callbacks, objtype, (void*) callback);
}

int cfg_parser_parse(struct cfg_parser_t *cp, char *filename, void *ctx) {
	int ret;

	cp->fp = fopen(filename, "r");
	cp->ctx = ctx;
	cp->bufpos = 0;
	cp->buflen = 0;
	/* Kickstart, so buflen != 0 when data still exist */
	cfg_parser_fill(cp);

	ret = cfg_parser_parse_entry(cp);

	fclose(cp->fp);
	cp->ctx = NULL;

	return ret;
}


static void cfg_parser_map_free_both_callback(const char*key, void*value, void*ctx) {
	free((void*)key);
	free(value);
}

void cfg_parser_destroy_args(struct strmap_t *args) {
	strmap_destroy(args, cfg_parser_map_free_both_callback, NULL);
}


/*******************
 * LOW LEVEL FILE ACCESS
 *******************/
static int cfg_parser_fill(struct cfg_parser_t *cp) {
	size_t read_start;
	size_t read_length;
	size_t data_received;
	if (cp->buflen >= CFG_PARSER_MIN_BUF_SIZE) {
		/* Buffer has data enough, which is no problem */
		return 0;
	}
	if (cp->bufpos + cp->buflen < CFG_PARSER_BUF_SIZE) {
		/* Fill to end of circular buffer, nessecary not filling buffer */
		read_start = cp->bufpos + cp->buflen;
		read_length = CFG_PARSER_BUF_SIZE - read_start;

		data_received = fread(&cp->buf[read_start], 1, read_length, cp->fp);
		if (data_received == 0) {
			/* EOF or error */
			return 1;
		}

		cp->buflen += data_received;
	}
	if (cp->bufpos + cp->buflen >= CFG_PARSER_BUF_SIZE) {
		/* Fill to buffer full */

		read_start = cp->bufpos + cp->buflen - CFG_PARSER_BUF_SIZE;
		read_length = CFG_PARSER_BUF_SIZE - cp->buflen;

		data_received = fread(&cp->buf[read_start], 1, read_length, cp->fp);
		if (data_received == 0) {
			/* EOF or error */
			return 1;
		}

		cp->buflen += data_received;
	}

	/* No problem if reached to this point */
	return 0;
}

static size_t cfg_parser_read(struct cfg_parser_t *cp, char *buf, size_t len) {
	size_t left;
	size_t read_length;
	cfg_parser_fill(cp);

	if (len > cp->buflen) {
		len = cp->buflen;
	}

	left = len;
	if (cp->bufpos + left >= CFG_PARSER_BUF_SIZE) {
		read_length = CFG_PARSER_BUF_SIZE - cp->bufpos;
		memcpy(buf, &cp->buf[cp->bufpos], CFG_PARSER_BUF_SIZE - cp->bufpos);
		left -= read_length;
		cp->buflen -= read_length;
		buf += read_length;
		cp->bufpos = 0;
	}
	if (left > 0) {
		memcpy(buf, &cp->buf[cp->bufpos], left);
		cp->buflen -= left;
		cp->bufpos += left;
	}

	return len;
}

static size_t cfg_parser_strip_ws(struct cfg_parser_t *cp) {
	size_t cnt;
	cfg_parser_fill(cp);

	while (cp->buflen
			&& (cp->buf[cp->bufpos] == ' ' || cp->buf[cp->bufpos] == '\t')) {
		cp->bufpos = (cp->bufpos + 1) % CFG_PARSER_BUF_SIZE;
		cp->buflen--;
		cnt++;
	}

	return cnt;
}

/*******************
 * DECENTEND RECURSIVE PARSER ACCEPTORS
 *******************/

static char *cfg_parser_accept_name(struct cfg_parser_t *cp) {
	size_t len;
	char *buf = NULL;
	char c;

	cfg_parser_strip_ws(cp);
	cfg_parser_fill(cp);

	/* Don't read anything yet, just track down the length to read */
	len = 0;
	while (len < cp->buflen) {
		c = cp->buf[(cp->bufpos + len) % CFG_PARSER_BUF_SIZE];
		if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_') {
			len++;
		} else {
			break;
		}
	}

	if (len == 0) {
		return NULL;
	}

	buf = malloc(len + 1);
	if (!buf) {
		/* FIXME: error handling */
		return NULL;
	}

	len = cfg_parser_read(cp, buf, len);
	buf[len] = '\0';

	return buf;
}

static char *cfg_parser_accept_line(struct cfg_parser_t *cp) {
	size_t len;
	char *buf = NULL;
	char c;

	cfg_parser_strip_ws(cp);
	cfg_parser_fill(cp);

	/* Don't read anything yet, just track down the length to read */
	len = 0;
	while (len < cp->buflen) {
		c = cp->buf[(cp->bufpos + len) % CFG_PARSER_BUF_SIZE];
		if (c != '\n' && c != '\r' && c != ';') {
			len++;
		} else {
			break;
		}
	}

	if (len == 0) {
		return NULL;
	}

	buf = malloc(len + 1);
	if (!buf) {
		/* FIXME: error handling */
		return NULL;
	}

	len = cfg_parser_read(cp, buf, len);
	/* strip whitespaces from end */
	while (len > 1 && (buf[len - 1] == ' ' || buf[len - 1] == '\t')) {
		len--;
	}
	buf[len] = '\0';

	/* Discard rest of line, including comment and linebreak */
	cfg_parser_accept_line_discard(cp);

	return buf;
}

static int cfg_parser_accept_eol(struct cfg_parser_t *cp) {
	char c;
	int got_nl = 0;

	cfg_parser_strip_ws(cp);
	cfg_parser_fill(cp);

	/* Strip newline from end */
	while (cp->buflen) {
		c = cp->buf[cp->bufpos];
		if (c != '\n' && c != '\r') {
			break;
		} else {
			cp->bufpos = (cp->bufpos + 1) % CFG_PARSER_BUF_SIZE;
			cp->buflen--;
			got_nl = 1;
		}
	}

	return got_nl;
}

static int cfg_parser_accept_line_discard(struct cfg_parser_t *cp) {
	char c;
	int got_nl = 0;

	cfg_parser_fill(cp);

	/* Strip newline from end */
	while (cp->buflen) {
		c = cp->buf[cp->bufpos];
		if (c != '\n' && c != '\r') {
			if (got_nl) {
				break;
			} else {
				cp->bufpos = (cp->bufpos + 1) % CFG_PARSER_BUF_SIZE;
				cp->buflen--;
			}
		} else {
			cp->bufpos = (cp->bufpos + 1) % CFG_PARSER_BUF_SIZE;
			cp->buflen--;
			got_nl = 1;
		}
	}

	return 1;
}
static int cfg_parser_accept_comment_discard(struct cfg_parser_t *cp) {
	char c;

	if (cfg_parser_accept_char(cp, ';')) {
		cfg_parser_fill(cp);

		/* Strip newline from end */
		while (cp->buflen) {
			c = cp->buf[cp->bufpos];
			if (c != '\n' && c != '\r') {
				cp->bufpos = (cp->bufpos + 1) % CFG_PARSER_BUF_SIZE;
				cp->buflen--;
			} else {
				break;
			}
		}
		return 1;
	}

	return 0;
}

static int cfg_parser_accept_word(struct cfg_parser_t *cp, char *word) {
	size_t ptr, len;

	cfg_parser_strip_ws(cp);
	cfg_parser_fill(cp);

	/* Iterate as long as chars are left in word */
	ptr = cp->bufpos;
	len = cp->buflen;
	while (*word) {
		if (len == 0) {
			/* No chars left in file/buffer*/
			return 0;
		}
		if (cp->buf[ptr] != *word) {
			/* Doesn't match */
			return 0;
		}
		word++;
		ptr = (ptr + 1) % CFG_PARSER_BUF_SIZE;
		len--;
	}

	/* Strip word from buffer */
	cp->bufpos = ptr;
	cp->buflen = len;

	return 1;
}

static int cfg_parser_accept_char(struct cfg_parser_t *cp, char c) {
	cfg_parser_strip_ws(cp);
	cfg_parser_fill(cp);

	if (cp->buflen && cp->buf[cp->bufpos] == c) {
		cp->bufpos = (cp->bufpos + 1) % CFG_PARSER_BUF_SIZE;
		cp->buflen--;
		return 1;
	}
	return 0;
}

/*******************
 * DECENTEND RECURSIVE PARSER RULES
 *******************/

static int cfg_parser_parse_object(struct cfg_parser_t *cp) {
	/* define is fetched, now time for an object definition */
	char *type = NULL;
	char *var = NULL;
	char *value = NULL;

	struct strmap_t *args = NULL;

	cfg_parser_callback_t cb;

	if (NULL == (type = cfg_parser_accept_name(cp))) {
		return 0;
	}

	if (!cfg_parser_accept_char(cp, '{')) {
		free(type);
		return 0;
	}

	cfg_parser_accept_comment_discard(cp);

	if (!cfg_parser_accept_eol(cp)) {
		free(type);
		return 0;
	}

	while (!cfg_parser_accept_char(cp, '}')) {
		/* Comment */
		if (cfg_parser_accept_char(cp, '#')) {
			cfg_parser_accept_line_discard(cp);
			continue;
		}

		var = cfg_parser_accept_name(cp);
		if (var) {
			value = cfg_parser_accept_line(cp);
			if (value) {
				if (strmap_add(&args, var, value)) {
					printf("Ignoring duplicate: %s = %s\n", var, value);
					free(var);
					free(value);
				}
			} else {
				free(var);
				/* FIXME: error */
				cfg_parser_accept_line_discard(cp);
			}
		} else {
			/* FIXME: error */
			cfg_parser_accept_line_discard(cp);
		}
	}

	cfg_parser_accept_comment_discard(cp);

	if (!cfg_parser_accept_eol(cp)) {
		free(type);
		return 0;
	}

	/* Search for callback */
	cb = (cfg_parser_callback_t)strmap_get(cp->callbacks, type);
	if (cb) {
		if (!(*cb)(type, args, cp->ctx)) {
			/* It didn't steal the args array */
			cfg_parser_destroy_args(args);
		}
	} else {
		/* Unknown type */
		cfg_parser_destroy_args(args);
	}

	free(type);
	return 1;
}

static int cfg_parser_parse_entry(struct cfg_parser_t *cp) {
	while (cp->buflen) {
		/* Comment */
		if (cfg_parser_accept_char(cp, '#')) {
			cfg_parser_accept_line_discard(cp);
			continue;
		}

		/* Object configuration */
		if (cfg_parser_accept_word(cp, "define")) {
			if (!cfg_parser_parse_object(cp)) {
				return 0;
			}
		}

		/* FIXME: error */
		cfg_parser_accept_line_discard(cp);
	}
	return 1;
}
