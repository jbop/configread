#include <stdio.h>
#include "cfgi.h"
#include "strmap.h"

static int print_timeperiod(const char *type, struct cfgi_obj_t *obj, void *ctx) {
	printf("timeperiod %s\n", cfgi_get_string(obj, "timeperiod_name"));
	return 0;
}
static int print_contact(const char *type, struct cfgi_obj_t *obj, void *ctx) {
	printf("contact %s\n", cfgi_get_string(obj, "contact_name"));
	return 0;
}
static int print_host(const char *type, struct cfgi_obj_t *obj, void *ctx) {
	printf("host %s\n", cfgi_get_string(obj, "host_name"));
	return 0;
}
static int print_service(const char *type, struct cfgi_obj_t *obj, void *ctx) {
	printf("service %-50s", cfgi_get_string(obj, "service_description"));
	printf(" notification period: %s\n", cfgi_get_string(obj, "notification_period"));
	return 0;
}

int main(void) {
	struct cfgi_t *cih;
	cih = cfgi_create();
	cfgi_register_handler(cih, "timeperiod", print_timeperiod);
	cfgi_register_handler(cih, "contact", print_contact);
	cfgi_register_handler(cih, "host", print_host);
	cfgi_register_handler(cih, "service", print_service);
	cfgi_parse(cih, "testfile.txt", NULL);
	cfgi_destroy(cih);
	return 0;
}
