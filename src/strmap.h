/*
 * strmap.h
 *
 *  Created on: Feb 2, 2014
 *      Author: msikstrom
 */

#ifndef STRMAP_H_
#define STRMAP_H_

#define STRMAP_OK 0
#define STRMAP_DUP 1
#define STRMAP_MEMERR 2

struct strmap_t;

int strmap_add(struct strmap_t **map, const char *key, void *value);
void *strmap_get(struct strmap_t *map, const char *key);
void strmap_destroy(struct strmap_t *map, void (*free_fnc)(const char*, void*, void*),
		void *ctx);
void strmap_iterate(struct strmap_t *map,
		void (*iterate_fnc)(const char*, void*, void*), void *ctx);

#endif /* STRMAP_H_ */
