#include "cfgi.h"
#include <stdlib.h>
#include "cfg_parser.h"
#include <string.h>
#include <stdio.h>

struct cfgi_obj_t {
	struct cfgi_obj_t *next;
	struct strmap_t *args;
	int register_object;

	/* Store a copy of the use-string, but with delimiters modified to \0 */
	char *use_store;
	/* Store pointers to start of each inheritance, in reverse order, NULL-terminated */
	char **use_list;

	/* Back reference, so args can be fetched only by an object */
	struct cfgi_type_t *type;
};

struct cfgi_type_t {
	struct cfgi_obj_t *objlist;
	struct strmap_t *templates;
	cfgi_callback_t callback;

	/* Back reference, so cfgi_get_List can accexs its temporary storage */
	struct cfgi_t *cih;
};

struct cfgi_t {
	void *ctx;
	struct cfg_parser_t *parser;

	/* Contains struct cfgi_type_t * */
	struct strmap_t *types;
};
static int cfgi_create_object(const char *objtype, struct strmap_t *args,
		void *ctx);
static void cfgi_destroy_object(struct cfgi_obj_t *obj);
static void cfgi_object_update_use(struct cfgi_obj_t *obj);

static struct cfgi_type_t *cfgi_type_create(struct cfgi_t *cih);
static void cfgi_type_destroy(const char *type, void *value, void *ctx);

static void cfgi_iterate_objects(const char *type, void *value, void *ctx);
static void cfgi_destroy_objects(const char *type, void *value, void *ctx);

/*************
 * Object context
 *************/

static int cfgi_create_object(const char *objtype, struct strmap_t *args,
		void *ctx) {
	struct cfgi_t *cih = (struct cfgi_t *) ctx;
	struct cfgi_type_t *cit = strmap_get(cih->types, objtype);
	struct cfgi_obj_t *obj = (struct cfgi_obj_t *) calloc(1,
			sizeof(struct cfgi_obj_t));

	const char *register_string = strmap_get(args, "register");
	const char *name = strmap_get(args, "name");

	obj->args = args;
	obj->type = cit;

	cfgi_object_update_use(obj);

	if (register_string == NULL) {
		obj->register_object = 1;
	} else {
		obj->register_object = (0 != atoi(register_string));
	}

	obj->next = cit->objlist;
	cit->objlist = obj;

	if (name != NULL) {
		strmap_add(&cit->templates, name, obj);
	}

	return 1; /* 1 means stealing the args resource */
}

static void cfgi_destroy_object(struct cfgi_obj_t *obj) {
	cfg_parser_destroy_args(obj->args);
	free(obj);
}

static void cfgi_object_update_use(struct cfgi_obj_t *obj) {
	char *use;
	char *tmp;
	char *last_char;
	size_t use_len;
	size_t use_ptr;
	use = strmap_get(obj->args, "use");
	if (!use) {
		return;
	}
	obj->use_store = strdup(use);

	use_len = 1;
	for (tmp = obj->use_store; *tmp; tmp++) {
		if (*tmp == ',') {
			use_len++;
		}
	}

	obj->use_list = calloc(use_len + 1, sizeof(char **));
	use_ptr = use_len - 1;

	tmp = obj->use_store;
	obj->use_list[use_ptr--] = tmp;
	while (*tmp) {
		if (*tmp == ',') {
			*tmp = '\0';
			obj->use_list[use_ptr--] = tmp + 1;
		}
		tmp++;
	}

	printf("use_len = %lu\n", use_len);
	for (use_ptr = 0; obj->use_list[use_ptr]; use_ptr++) {
		printf("Use string: '%s'\n", obj->use_list[use_ptr]);
	}

	for (use_ptr = 0; obj->use_list[use_ptr]; use_ptr++) {
		/* Trim start */
		tmp = obj->use_list[use_ptr];
		while (*tmp) {
			if (*tmp == ' ' || *tmp == '\t') {
				tmp++;
			} else {
				break;
			}
		}
		obj->use_list[use_ptr] = tmp;

		/* Trim end */
		last_char = tmp;
		while (*tmp) {
			if (*tmp != ' ' && *tmp != '\t') {
				last_char = tmp;
			}
			tmp++;
		}
		if (*last_char) {
			*(last_char + 1) = '\0';
		}
	}

	for (use_ptr = 0; obj->use_list[use_ptr]; use_ptr++) {
		printf("Trimmed: '%s'\n", obj->use_list[use_ptr]);
	}

}

const char *cfgi_get_string(struct cfgi_obj_t *obj, const char *name) {
	const char *value;
	struct cfgi_obj_t *use;
	char **use_ptr;

	value = strmap_get(obj->args, name);

	use_ptr = obj->use_list;
	for (use_ptr = obj->use_list; !value && use_ptr && *use_ptr; use_ptr++) {
		use = strmap_get(obj->type->templates, *use_ptr);
		if (use) {
			value = cfgi_get_string(use, name);
		} else {
			/* FIXME: error handling, unknown template */
		}
	}
	return value;
}

long cfgi_get_long(struct cfgi_obj_t *obj, const char *name) {
	const char *str = cfgi_get_string(obj, name);
	return atol(str);
}

const char **cfgi_get_list(struct cfgi_obj_t *obj, const char *name) {
	/*char *value;
	 struct cfgi_obj_t *use;
	 struct cfgi_t *cih = obj->type->cih;

	 value = strmap_get(obj->args, name);
	 if (value) {
	 return value;
	 }
	 if (obj->use_store) {
	 use = strmap_get(obj->type->templates, obj->use_store);
	 if (use) {
	 return cfgi_get_string(use, name);
	 } else {
	 }
	 }*/
	return NULL;
}

/*************
 * Object type context
 *************/
static struct cfgi_type_t *cfgi_type_create(struct cfgi_t *cih) {
	struct cfgi_type_t *ct = (struct cfgi_type_t *) calloc(1,
			sizeof(struct cfgi_type_t));
	ct->cih = cih;
	return ct;
}

static void cfgi_type_destroy(const char *type, void *value, void *ctx) {
	struct cfgi_type_t *cit = (struct cfgi_type_t *) value;
	strmap_destroy(cit->templates, NULL, NULL);
	cfgi_destroy_objects("", (void*) cit, NULL);
	free(cit);
}

static void cfgi_iterate_objects(const char *type, void *value, void *ctx) {
	struct cfgi_t *cih = (struct cfgi_t *) ctx;
	struct cfgi_type_t *cit = (struct cfgi_type_t *) value;

	struct cfgi_obj_t *curobj;

	for (curobj = cit->objlist; curobj; curobj = curobj->next) {
		if (curobj->register_object) {
			(*cit->callback)(type, curobj, cih->ctx);
		}
	}
}

static void cfgi_destroy_objects(const char *type, void *value, void *ctx) {
	struct cfgi_type_t *cit = (struct cfgi_type_t *) value;

	struct cfgi_obj_t *nextobj;

	while (cit->objlist) {
		nextobj = cit->objlist->next;
		cfgi_destroy_object(cit->objlist);
		cit->objlist = nextobj;
	}
}

int cfgi_register_handler(struct cfgi_t *cih, char *objtype,
		cfgi_callback_t callback) {
	struct cfgi_type_t *cit;

	cfg_parser_register_handler(cih->parser, objtype, cfgi_create_object);

	cit = cfgi_type_create(cih);
	cit->callback = callback;
	strmap_add(&cih->types, objtype, cit);
	/* FIXME: error handling */
	return 0;
}

/*************
 * Inheritance context
 *************/
struct cfgi_t *cfgi_create(void) {
	struct cfgi_t *cih = calloc(1, sizeof(struct cfgi_t));
	cih->parser = cfg_parser_create();
	return cih;
}

void cfgi_destroy(struct cfgi_t *cih) {
	strmap_destroy(cih->types, cfgi_type_destroy, NULL);
	cfg_parser_destroy(cih->parser);
	free(cih);
}

void cfgi_parse(struct cfgi_t *cih, char *filename, void *ctx) {
	cih->ctx = ctx;
	cfg_parser_parse(cih->parser, filename, (void*) cih);
	strmap_iterate(cih->types, cfgi_iterate_objects, cih);
	strmap_iterate(cih->types, cfgi_destroy_objects, NULL);
	cih->ctx = NULL;
}
